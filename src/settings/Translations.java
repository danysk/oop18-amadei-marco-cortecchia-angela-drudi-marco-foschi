package settings;

/**
 * 
 * @author Angela Cortecchia
 *
 */

import gui.TextToTranslation;

public class Translations {
	
	public enum Language
	{
		ITALIAN,
		ENGLISH,
		GERMAN,
		SPANISH,
		FRENCH;
	}

	public static Language language= Language.ITALIAN;
	
	public static void setlanguage(Language lang) {
		language = lang;
    	TextToTranslation.updateAllTranslations();
	}
	
	public static String getTranslation(String textID)
	{
		switch (textID)
		{
			default:
				return "wrong-id";

			case "help1":
				switch(language) {
					case ITALIAN:
						return "<html>Clicca su una tua pedina e ti appariranno dei suggerimenti di possibili<br>"
								+ " mosse evidenziando le celle in giallo, successivamente cliccando su<br>"
								+ " una delle celle evidenziate in giallo la pedina si spostera in quella cella.</html>";
					case ENGLISH:
						return "<html>Click on one of your pawns and you will see suggestions of possible moves<br>"
								+ " by highlighting the cells in yellow, then clicking on one of the cells<br>"
								+ " highlighted in yellow the pawn will move into that cell.</html>";
					case GERMAN:
						return "<html>Klicken Sie auf einen Ihrer Bauern, und Sie sehen Vorschläge für mögliche Züge<br>"
								+ ", indem Sie die Zellen gelb markieren. Klicken Sie anschließend auf eine der<br>"
								+ " gelb markierten Zellen, um den Bauern in diese Zelle zu bewegen.</html>";
					case SPANISH:
						return "<html>Haga clic en uno de sus peones y verá sugerencias de posibles movimientos<br>"
								+ " resaltando las celdas en amarillo, luego haciendo clic en una de las celdas<br>"
								+ " resaltadas en amarillo, el peón se moverá a esa celda.</html>";
					case FRENCH:
						return "<html>Cliquez sur l'un de vos pions et vous verrez des suggestions de mouvements<br>"
								+ " possibles en mettant en surbrillance les cellules en jaune, puis en cliquant<br>"
								+ " sur l'une des cellules surlignées en jaune, le pion se déplacera dans cette cellule.</html>";
					default:
						throw new IllegalStateException();
				}
				
			case "checkMate":
				switch(language) {
				case ITALIAN:
					return "Scacco matto!";
				case ENGLISH:
					return "Checkmate!";
				case GERMAN:
					return "Schachmatt!";
				case SPANISH:
					return "Jaque mate!";
				case FRENCH:
					return "Éches et matt!";
				default:
					throw new IllegalStateException();
				}
				
			case "settings":
				switch(language) {
				case ITALIAN:
					return "Impostazioni";
				case ENGLISH:
					return "Settings";
				case GERMAN:
					return "Einstellungen";
				case SPANISH:
					return "Configuración";
				case FRENCH:
					return "Paramètres";
				default:
					throw new IllegalStateException();
				}
				
			case "turn":
					switch(language) {
					case ITALIAN:
						return "Turno";
					case ENGLISH:
						return "Turn";
					case GERMAN:
						return "Dran";
					case SPANISH:
						return "Turno";
					case FRENCH:
						return "Tour";
					default:
						throw new IllegalStateException();
					}
					
			case "Statistics":
				switch(language){
					case ITALIAN:
						return "Dati statistici";
					case ENGLISH:
						return "Statistics";
					case GERMAN:
						return "Statistiken";
					case SPANISH:
						return "Datos estadísticos";
					case FRENCH:
						return "Statistiques";
					default:
						throw new IllegalStateException();
				}
				
			case "statistics_title":
				switch(language){
					case ITALIAN:
						return "Statistiche";
					case ENGLISH:
						return "Statistics";
					case GERMAN:
						return "Statistiken";
					case SPANISH:
						return "datos estadísticos";
					case FRENCH:
						return "Statistiques";
					default:
						throw new IllegalStateException();
				}
					

			case "Bishop.png":
				switch(language) {
					case ITALIAN:
						return "Alfiere";
					case ENGLISH:
						return "Bishop";
					case GERMAN:
						return "Läufer";
					case SPANISH:
						return "Perón";
					case FRENCH:
						return "Pion";
					default:
						throw new IllegalStateException();
				}
				
			case "King.png":
				switch(language) {
				case ITALIAN:
					return "Re";
				case ENGLISH:
					return "King";
				case GERMAN:
					return "König";
				case SPANISH:
					return "Rey";
				case FRENCH:
					return "Roi";
				default:
					throw new IllegalStateException();
				}
				
			case "Queen.png":
				switch(language) {
				case ITALIAN:
					return "Regina";
				case ENGLISH:
					return "Queen";
				case GERMAN:
					return "Dame";
				case SPANISH:
					return "Reina";
				case FRENCH:
					return "Dame";
				default:
					throw new IllegalStateException();
				}
				
			case "Knight.png":
				switch(language) {
				case ITALIAN:
					return "Cavallo";
				case ENGLISH:
					return "Knight";
				case GERMAN:
					return "Läufer";
				case SPANISH:
					return "Alfil";
				case FRENCH:
					return "Fou";
				default:
					throw new IllegalStateException();
				}
				
			case "Rook.png":
				switch(language) {
				case ITALIAN:
					return "Torre";
				case ENGLISH:
					return "Rook";
				case GERMAN:
					return "Turm";
				case SPANISH:
					return "Torre";
				case FRENCH:
					return "Tour";
				default:
					throw new IllegalStateException();
				}
				
			case "Pawn.png":
				switch(language) {
				case ITALIAN:
					return "Pedone";
				case ENGLISH:
					return "Pawn";
				case GERMAN:
					return "Bauer";
				case SPANISH:
					return "Perón";
				case FRENCH:
					return "Pion";
				default:
					throw new IllegalStateException();
				}
				
			case "chess":
				switch(language) {
				case ITALIAN:
					return "Scacchi";
				case ENGLISH:
					return "Chess";
				case GERMAN:
					return "Schach";
				case SPANISH:
					return "Ajedrez";
				case FRENCH:
					return "Èchecs";
				default:
					throw new IllegalStateException();
				}
				
			case "player":
				switch(language) {
				case ITALIAN:
					return "Giocatore";
				case ENGLISH:
					return "Player";
				case GERMAN:
					return "Spieler";
				case SPANISH:
					return "Jugador";
				case FRENCH:
					return "Joueur";
				default:
					throw new IllegalStateException();
				}
				
			case "help":
				switch(language) {
				case ITALIAN:
					return "Aiuto";
				case ENGLISH:
					return "Help";
				case GERMAN:
					return "Hilfe";
				case SPANISH:
					return "Ayuda";
				case FRENCH:
					return "Aide";
				default:
					throw new IllegalStateException();
				}
			
			case "customize":
				switch(language) {
				case ITALIAN:
					return "Personalizzazione";
				case ENGLISH:
					return "Customize";
				case GERMAN:
					return "Personalizieren";
				case SPANISH:
					return "Personalizar";
				case FRENCH:
					return "Personnaliser";
				default:
					throw new IllegalStateException();
				}
				
			case "hint":
				switch(language) {
				case ITALIAN:
					return "Suggerimento";
				case ENGLISH:
					return "Hint";
				case GERMAN:
					return "Tipp";
				case SPANISH:
					return "Pista";
				case FRENCH:
					return "Conseil";
				default:
					throw new IllegalStateException();
				}
				
			case "rematch":
				switch(language) {
				case ITALIAN:
					return "Rivincita";
				case ENGLISH:
					return "Rematch";
				case GERMAN:
					return "Revanche";
				case SPANISH:
					return "Revancha";
				case FRENCH:
					return "Rache";
				default:
					throw new IllegalStateException();
				}
				
			case "newgame":
				switch(language) {
				case ITALIAN:
					return "Nuova partita";
				case ENGLISH:
					return "New game";
				case GERMAN:
					return "Neue Schachspiel";
				case SPANISH:
					return "Nueva partida";
				case FRENCH:
					return "Nouveau jeu";
				default:
					throw new IllegalStateException();
				}
				
			case "history":
				switch(language) {
				case ITALIAN:
					return "Storico partite";
				case ENGLISH:
					return "History";
				case GERMAN:
					return "Historiker-Spiele";
				case SPANISH:
					return "Históricos partidos";
				case FRENCH:
					return "Match historiques";
				default:
					throw new IllegalStateException();
				}
					
			case "close":
				switch(language) {
					case ITALIAN:
						return "Chiudi";
					case ENGLISH:
						return "Close";
					case GERMAN:
						return "Schließen";
					case SPANISH:
						return "Cerrar";
					case FRENCH:
						return "Fermer";
					default:
						throw new IllegalStateException();
				}
			case "pause":
				switch(language) {
				case ITALIAN:
					return "Pausa";
				case ENGLISH:
					return "Pause";
				case GERMAN:
					return "Pause";
				case SPANISH:
					return "Pausa";
				case FRENCH:
					return "Pause";
				default:
					throw new IllegalStateException();
				}
			
			case "play":
				switch(language) {
				case ITALIAN:
					return "Riprendi";
				case ENGLISH:
					return "Play";
				case GERMAN:
					return "Wiederaufnehmen";
				case SPANISH:
					return "Reprender";
				case FRENCH:
					return "Reprenez";
				default:
					throw new IllegalStateException();
				}
				
			case "restart":
				switch(language) {
				case ITALIAN:
					return "Ricomincia";
				case ENGLISH:
					return "Restart";
				case GERMAN:
					return "Wiederanfangen";
				case SPANISH:
					return "Reinicio";
				case FRENCH:
					return "Recomence";
				default:
					throw new IllegalStateException();
				}
				
			case "win":
				switch(language) {
				case ITALIAN:
					return "Hai vinto";
				case ENGLISH:
					return "You won";
				case GERMAN:
					return "Du hast gewonnen";
				case SPANISH:
					return "Tú has ganado";
				case FRENCH:
					return "Tu as gagné";
				default:
					throw new IllegalStateException();
				}
				
			case "lose":
				switch(language) {
				case ITALIAN:
					return "Hai perso";
				case ENGLISH:
					return "You lost";
				case GERMAN:
					return "Du hast verloren";
				case SPANISH:
					return "Tú has perdido";
				case FRENCH:
					return "Tu as perdú";
				default:
					throw new IllegalStateException();
				}
				
			case "points":
				switch(language) {
				case ITALIAN:
					return "Punti";
				case ENGLISH:
					return "Points";
				case GERMAN:
					return "Punkte";
				case SPANISH:
					return "Puntos";
				case FRENCH:
					return "Pois";
				default:
					throw new IllegalStateException();
				}
			
			case "keep playing":
				switch (language) {
				case ITALIAN:
					return "Continua a giocare";
				case ENGLISH:
					return "Keep playing";
				case GERMAN:
					return "Weiterspielen";
				case SPANISH:
					return "Seguir jugando";
				case FRENCH:
					return "Continuer à jouer";
				default: throw new IllegalStateException();
				}
				
			case "score":
				switch(language) {
				case ITALIAN:
					return "Punteggio";
				case ENGLISH:
					return "Score";
				case GERMAN:
					return "Spielstände";
				case SPANISH:
					return "Marcador";
				case FRENCH:
					return "Score";
				default:
					throw new IllegalStateException();
				}
				
			case "rank":
				switch(language) {
				case ITALIAN:
					return "Classifica";
				case ENGLISH:
					return "Ranking";
				case GERMAN:
					return "Rangliste";
				case SPANISH:
					return "Clasificación";
				case FRENCH:
					return "Classement";
				default:
					throw new IllegalStateException();
				}
				
			case "board":
				switch(language) {
				case ITALIAN:
					return "Scacchiera";
				case ENGLISH:
					return "Board";
				case GERMAN:
					return "Schachbrett";
				case SPANISH:
					return "Tablero";
				case FRENCH:
					return "Échiquier";
				default:
					throw new IllegalStateException();
				}
				
			case "insertname":
				switch(language) {
				case ITALIAN:
					return "Inserisci il tuo nome";
				case ENGLISH:
					return "Write your name";
				case GERMAN:
					return "Screib deinen Namen";
				case SPANISH:
					return "Escribe tu nombre";
				case FRENCH:
					return "Écrivez votre nom";
				default:
					throw new IllegalStateException();
				}
				
			case "selectlanguage":
				switch(language) {
				case ITALIAN:
					return "Scegli la lingua";
				case ENGLISH:
					return "Choose the language";
				case GERMAN:
					return "Wählen Sie die Sprache";
				case SPANISH:
					return "Elija el idioma";
				case FRENCH:
					return "Choix de la langue";
				default:
					throw new IllegalStateException();
				} 
			
			case "Lose" :
				switch(language) {
				case ITALIAN:
					return "Hai perso";
				case ENGLISH:
					return "You lost";
				case GERMAN:
					return "Du hast verloren";
				case SPANISH:
					return "Tú has perdido";
				case FRENCH:
					return "Tu as perdú";
				default:
					throw new IllegalStateException();
				}
				
				
			case "Points":
				switch(language) {
				case ITALIAN:
					return "Punti";
				case ENGLISH:
					return "Points";
				case GERMAN:
					return "Punkte";
				case SPANISH:
					return "Puntos";
				case FRENCH:
					return "Pois";
				default:
					throw new IllegalStateException();
				}
				
			case "Score":
				switch(language) {
				case ITALIAN:
					return "Punteggio";
				case ENGLISH:
					return "Score";
				case GERMAN:
					return "Spielstände";
				case SPANISH:
					return "Marcador";
				case FRENCH:
					return "Score";
				default:
					throw new IllegalStateException();
				}
				
			case "Rank":
				switch(language) {
				case ITALIAN:
					return "Classifica";
				case ENGLISH:
					return "Ranking";
				case GERMAN:
					return "Rangliste";
				case SPANISH:
					return "Clasificación";
				case FRENCH:
					return "Classement";
				default:
					throw new IllegalStateException();
				}
					
			case "Board":
				switch(language) {
				case ITALIAN:
					return "Scacchiera";
				case ENGLISH:
					return "Board";
				case GERMAN:
					return "Schachbrett";
				case SPANISH:
					return "Tablero";
				case FRENCH:
					return "Échiquier";
				default:
					throw new IllegalStateException();
				}
				
			case "Insert Name":
				switch(language) {
				case ITALIAN:
					return "Inserisci il tuo nome";
				case ENGLISH:
					return "Write your name";
				case GERMAN:
					return "Screib deinen Namen";
				case SPANISH:
					return "Escribe tu nombre";
				case FRENCH:
					return "Écrivez votre nom";
				default:
					throw new IllegalStateException();
				}
				
			case "Language":
				switch(language) {
				case ITALIAN:
					return "Scegli la lingua";
				case ENGLISH:
					return "Choose the language";
				case GERMAN:
					return "Wählen Sie die Sprache";
				case SPANISH:
					return "Elija el idioma";
				case FRENCH:
					return "Choix de la langue";
				default:
					throw new IllegalStateException();
				}
				
			case "Stalemate":
				switch(language) {
				case ITALIAN:
					return "Sei arrivato ad un punto di stallo!";
				case ENGLISH:
					return "Stalemate!";
				case GERMAN:
					return "Patt!";
				case SPANISH:
					return "Tablas por ahogado!";
				case FRENCH:
					return "Position de pat!";
				default:
					throw new IllegalStateException();
				}
				
			case "Winner":
				switch(language) {
				case ITALIAN:
					return "Vincitore";
				case ENGLISH:
					return "Winner";
				case GERMAN:
					return "Gewinner";
				case SPANISH:
					return "Ganador";
				case FRENCH:
					return "Gagnant";
				default:
					throw new IllegalStateException();
				}
				
			case "Time":
				switch(language) {
				case ITALIAN:
					return "Tempo";
				case ENGLISH:
					return "Time";
				case GERMAN:
					return "Zeit";
				case SPANISH:
					return "Tiempo";
				case FRENCH:
					return "Temps";
				default:
					throw new IllegalStateException();
				}
				
			case "Enable":
				switch(language) {
				case ITALIAN:
					return "Attiva";
				case ENGLISH:
					return "Enable";
				case GERMAN:
					return "Aktivieren";
				case SPANISH:
					return "Activar";
				case FRENCH:
					return "Activer";
				default:
					throw new IllegalStateException();
				}
				
			case "Dimension":
				switch(language) {
				case ITALIAN:
					return "Dimensione";
				case ENGLISH:
					return "Dimension";
				case GERMAN:
					return "Dimension";
				case SPANISH:
					return "Dimensión";
				case FRENCH:
					return "Dimension";
				default:
					throw new IllegalStateException();
				}

			case "difficulty":
				switch(language) {
				case ITALIAN:
					return "Difficoltà";
				case ENGLISH:
					return "Difficulty";
				case GERMAN:
					return "Schwierigkeit";
				case SPANISH:
					return "Dificultad";
				case FRENCH:
					return "Difficulté";
				default:
					throw new IllegalStateException();
			}
				
			case "timesettings":
				switch(language) {
					case ITALIAN:
						return "Cronometro";
					case ENGLISH:
						return "Time Settings";
					case GERMAN:
						return "Stoppuhr";
					case SPANISH:
						return "Cronógrafo";
					case FRENCH:
						return "Chronométre";
					default:
						throw new IllegalStateException();
				}
					
			case "skinpack":
					return "Skin Pack";
					
			case "player1name":
				switch(language) {
					case ITALIAN:
						return "Nome Giocatore 1";
					case ENGLISH:
						return "Player 1 Name";
					case GERMAN:
						return "Name Spieler 1";
					case SPANISH:
						return "Nombre jugador 1";
					case FRENCH:
						return "Nom joueur 1";
					default:
						throw new IllegalStateException();
				}

			case "back":
				switch(language) {
				case ITALIAN:
					return "Indietro";
				case ENGLISH:
					return "Back";
				case GERMAN:
					return "Zurück";
				case SPANISH:
					return "Atrás";
				case FRENCH:
					return "Retour";
				default:
					throw new IllegalStateException();
				}
			
			case "player2name":
				switch(language) {
				case ITALIAN:
					return "Nome Giocatore 2";
				case ENGLISH:
					return "Player 2 Name";
				case GERMAN:
					return "Name Spieler 2";
				case SPANISH:
					return "Nombre jugador 2";
				case FRENCH:
					return "Nom joueur 2";
				default:
					throw new IllegalStateException();
				}
			case "isai":
				switch(language) {
					case ITALIAN:
						return "E' IA";
					case ENGLISH:
						return "Is AI";
					case GERMAN:
						return "Ist AI";
					case SPANISH:
						return "Es AI";
					case FRENCH:
						return "Est l'IA";
					default:
						throw new IllegalStateException();
				}

			case "matches":
				switch(language) {
					case ITALIAN:
						return "Partite";
					case ENGLISH:
						return "Matches";
					case GERMAN:
						return "Streichhölzer";
					case SPANISH:
						return "Cerillas";
					case FRENCH:
						return "Allumettes";
					default:
						throw new IllegalStateException();
				}
				
			case "white":
				switch(language) {
					case ITALIAN:
						return "BIANCHI";
					case ENGLISH:
						return "WHITE";
					case GERMAN:
						return "WEISS";
					case SPANISH:
						return "BLANCO";
					case FRENCH:
						return "BLANC";
					default:
						throw new IllegalStateException();
				}

			case "black":
				switch(language) {
					case ITALIAN:
						return "NERI";
					case ENGLISH:
						return "BLACK";
					case GERMAN:
						return "SCHWARZ";
					case SPANISH:
						return "NEGRO";
					case FRENCH:
						return "NOIRE";
					default:
						throw new IllegalStateException();
				}


			case "remainingtime":
				switch(language) {
					case ITALIAN:
						return "Tempo Rimanente";
					case ENGLISH:
						return "Remaining Time";
					case GERMAN:
						return "Verbleibende Zeit";
					case SPANISH:
						return "Tiempo restante";
					case FRENCH:
						return "Temps restant";
					default:
						throw new IllegalStateException();
				}	


			case "surrender":
				switch(language) {
					case ITALIAN:
						return "Arrenditi";
					case ENGLISH:
						return "Surrender";
					case GERMAN:
						return "Kapitulation";
					case SPANISH:
						return "Rendición";
					case FRENCH:
						return "Abandon";
					default:
						throw new IllegalStateException();
				}

			case "eatlist":
				switch(language) {
					case ITALIAN:
						return "Lista Mangiate";
					case ENGLISH:
						return "Eat List";
					case GERMAN:
						return "Eated Liste";
					case SPANISH:
						return "Lista comido";
					case FRENCH:
						return "Liste mangé";
					default:
						throw new IllegalStateException();
				}

			case "hintsenabled":
				switch(language) {
					case ITALIAN:
						return "Abilita Aiuti";
					case ENGLISH:
						return "Hints Enabled";
					case GERMAN:
						return "Hilfe aktivieren";
					case SPANISH:
						return "Habilitar ayuda";
					case FRENCH:
						return "Activer l'aide";
					default:
						throw new IllegalStateException();
				}

			case "soundsenabled":
				switch(language) {
					case ITALIAN:
						return "Abilita Suoni";
					case ENGLISH:
						return "Sounds Enabled";
					case GERMAN:
						return "Sounds Aktiviert";
					case SPANISH:
						return "Sonidos Habilitados";
					case FRENCH:
						return "Sons Activés";
					default:
						throw new IllegalStateException();
				}

			case "clear":
				switch(language) {
					case ITALIAN:
						return "Elimina Storico";
					case ENGLISH:
						return "Delete Hitory";
					case GERMAN:
						return "Verlauf löschen";
					case SPANISH:
						return "Borrar historial";
					case FRENCH:
						return "Effacer l'historique";
					default:
						throw new IllegalStateException();
				}

			case "nohistory":
				switch(language) {
					case ITALIAN:
						return "Nessuna partita trovata";
					case ENGLISH:
						return "No history found";
					case GERMAN:
						return "Keine Geschichte gefunden";
					case SPANISH:
						return "No se encontré historia";
					case FRENCH:
						return "Aucun historique trouvé";
					default:
						throw new IllegalStateException();
				}

			case "continuegame":
				switch(language) {
					case ITALIAN:
						return "Continua a giocare";
					case ENGLISH:
						return "Keep playing";
					case GERMAN:
						return "Weiter spielen";
					case SPANISH:
						return "Sigue jugando";
					case FRENCH:
						return "Continue à jouer";
					default:
						throw new IllegalStateException();
				}
				case "no2ai":
					switch(language) {
						case ITALIAN:
							return "Non puoi creare una partita con 2 IA!";
						case ENGLISH:
							return "You cannot create a match with 2 AIs!";
						case GERMAN:
							return "Sie können keine Übereinstimmung mit 2 AIs erstellen!";
						case SPANISH:
							return "¡No puedes crear una partida con 2 IA!";
						case FRENCH:
							return "Vous ne pouvez pas créer une correspondance avec 2 IA!r";
						default:
							throw new IllegalStateException();
					}

				case "cantmovethere":
					switch(language) {
						case ITALIAN:
							return "Questo pezzo non puo' muoversi lì!";
						case ENGLISH:
							return "This piece cannot move there!";
						case GERMAN:
							return "Dieses Stück kann sich dort nicht bewegen!";
						case SPANISH:
							return "¡Esta pieza no puede moverse allì!";
						case FRENCH:
							return "Cette piéce ne peut pas bouger là!";
						default:
							throw new IllegalStateException();
					}

				case "stillinchess":
					switch(language) {
						case ITALIAN:
							return "Dopo questa mossa, il re sarebbe in scacco!";
						case ENGLISH:
							return "After this move, the king would be in chess!";
						case GERMAN:
							return "Nach diesem Zug wäre der König in Schach!";
						case SPANISH:
							return "¡Después de este movimiento, el rey estarìa bajo control!";
						case FRENCH:
							return "Aprés ce mouvement, le roi serait en échec!";
						default:
							throw new IllegalStateException();
					}
		}
	}
}