package settings;

public interface GameSettings {
	
	/**
	 * 
	 * @author Angela Cortecchia
	 *
	 */

	
	enum Difficulty{
		EASY,
		MEDIUM,
		HARD;
	}
	
	Difficulty setDifficulty();
	
	public enum GameMode {
		STANDARD
	}
	
	GameMode setGameMode();
	
	enum Players{
		ONEVSONE,
		ONEVSAI,
		AIVSAI,
		FOURPLAYERS
	}
	
	Players setPlayers();
	
	enum BoardColors{
		WHITE,
		BLACK,
		BLUE,
		YELLOW,
		ORANGE,
		RED,
		LIGHTBLUE,
		PINK;
	}
	
	BoardColors setColor();
	
	public enum GameTime {
		DISABLED,
		MAX10SECONDS,
		MAX30SECONDS,
		MAX2MINUTES,
		MAX5MINUTES,
		MAX10MINUTES,
	}
	
	GameTime setGameTime();
	
	public enum PiecesSkin {
		STANDARD,
		CORVOSTUDIO
	}
	
	PiecesSkin setPieceSkin();
}
