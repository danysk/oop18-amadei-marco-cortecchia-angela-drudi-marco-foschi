package settings;

/**
 * 
 * @author Marco Amadei
 * 
 * Classe per accedere alle risorse indipendentemente dalla piattaforma su cui ci si trova
 *
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class CrossPlatformResources {

    /**
     * Translate a path to a resource into a path for the current OS platform..
     * @param resource The path for the resource into the project.
     * @return A File representation of the searched resource.
     */
	public static File getResourceFile(String resource){
		 File file = null;
		    Class<? extends CrossPlatformResources> dirClass=new CrossPlatformResources().getClass();
		    URL res = dirClass.getResource(resource);
		    if (isJAR()) {
		        try {
		            InputStream input = dirClass.getResourceAsStream(resource);
		            file = File.createTempFile("tempfile", ".tmp");
		            OutputStream out = new FileOutputStream(file);
		            int read;
		            byte[] bytes = new byte[1024];

		            while ((read = input.read(bytes)) != -1) {
		                out.write(bytes, 0, read);
		            }
		            out.close();
		            file.deleteOnExit();
		        } catch (IOException ex) {
		        	ex.printStackTrace();
		        }
		    } else {
		        file = new File(res.getPath());
		    }

		    if (file != null && !file.exists()) {
		        throw new RuntimeException("Error: File " + file + " not found!");
		    }

			//System.out.println("Carico da: "+file.getPath());
		return file;
	}

    /**
     * @return True if game has started from a compiled JAR, false otherwise.
     */
	public static boolean isJAR(){
		/*String protocol = new CrossPlatformResources().getClass().getResource("").getProtocol();
		return Objects.equals(protocol, "jar");*/
		String resourcePath=CrossPlatformResources.class.getResource("CrossPlatformResources.class").toString();
		return (resourcePath.startsWith("jar:") || resourcePath.startsWith("rsrc:"));
	}
}
