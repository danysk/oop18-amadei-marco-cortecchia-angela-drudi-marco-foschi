package board;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import piece.Cell;
import piece.Piece;
import piece.Queen;
import piece.Rook;
import player.PlayerColor;

class BoardTest {

	@Test
	void checkSomeCellsInStdBoard() throws Exception {
		GenericBoard b = new GenericBoard(BoardSchema.buildBoardStandard());
		assertEquals(Optional.empty(), b.getBoardState().get(3).get(5).getPiece());
		assertNotEquals(Optional.empty(), b.getBoardState().get(0).get(5).getPiece());
	}
	
	@Test
	void stdBoardIsSquare8by8() throws Exception {
		//voglio controllare che la std board venga sempre create quadrata
		ArrayList<ArrayList<Cell>> bs = BoardSchema.buildBoardStandard();
		int numRows = bs.size();
		assertEquals(numRows, 8);
		int numCols = numRows;
		for(List<Cell> r : bs) 
			assertEquals(r.size() , numCols);
		
	}
	

	@Test
	void checkBoardPositions() throws Exception{
		GenericBoard b = new GenericBoard(BoardSchema.buildBoardStandard());
		Optional<Piece> p;
		BoardPosition bp;

		//pezzo qualsiasi
		bp = new BoardPosition( "a", 1);
		
		p = b.getPieceAt(bp);
		assertFalse(!p.isPresent());
		assertTrue(p.get() instanceof Rook );
		assertEquals(p.get().getOwner(), PlayerColor.WHITE );
		
		//pezzo regine bianca
		bp = new BoardPosition( "d", 1);
		p = b.getPieceAt(bp);
		assertFalse( !p.isPresent() );
		assertTrue(p.get() instanceof Queen );
		assertEquals(p.get().getOwner(), PlayerColor.WHITE );
	
		//pezzo regine niera
		bp = new BoardPosition( "d", 8);
		p = b.getPieceAt(bp);
		assertFalse( !p.isPresent() );
		assertTrue(p.get() instanceof Queen );
		assertEquals(p.get().getOwner(), PlayerColor.BLACK );
	
	
	}
	
	
	
	@Test
	void checkPositionToStringConversion() throws Exception{
		BoardPosition bp = new BoardPosition( "d", 8);
		BoardPosition bp1 = new BoardPosition( "f", 4);

		assertTrue(bp.toString().contentEquals("d8"));
		assertTrue(bp1.toString().contentEquals("f4"));
		
	}	
	
	@SuppressWarnings("deprecation")
	@Test
	void checkInitialStringification() throws Exception{
		StandardBoard b = new StandardBoard();
		
		String fen = b.stringifyStateForAI1v1(
				PlayerColor.WHITE,
				true, 
				true, 
				true, 
				true, 
				null, 
				0);

		assertEquals(fen, "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
		
		b.movePiece(new BoardPosition("e", 2), new BoardPosition("e", 4));
		
		fen = b.stringifyStateForAI1v1(
				PlayerColor.BLACK,
				true, 
				true, 
				true, 
				true, 
				new BoardPosition("e", 3), 
				5);
		
		System.out.println(fen);
		// rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
	}
	
}
