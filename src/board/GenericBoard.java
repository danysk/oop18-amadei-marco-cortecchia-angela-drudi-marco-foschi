package board;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import piece.Bishop;
import piece.Cell;
import piece.King;
import piece.Knight;
import piece.Piece;
import piece.Queen;
import piece.Rook;
import player.PlayerColor;


/**
 * the class the represents the chess board
 * with some utilities
 */
@SuppressWarnings("deprecation")
public class GenericBoard implements Board{
	
	private Integer numColumns;
	private Integer numRows;
	private ArrayList<ArrayList<Cell>> boardState;
	int movesDone = 0;

	public GenericBoard( ArrayList<ArrayList<Cell>> board ) {
		this.boardState = board;
		this.setColumnsAndRows();
	}
	
	
	/**
	 * @returns the piece (if present) at the given position
	 */
	public Optional<Piece> getPieceAt(BoardPosition bp) {
		ArrayList<Cell> row = this.boardState.get(bp.getX());
		Optional<Piece> op = row.get(bp.getY()).getPiece();
		return op;
	}
	
	/**
	 * @returns the piece's color (if present) at the given position
	 */
	public Optional<PlayerColor> getPieceColorAtPosition(BoardPosition bp) {
		Optional<Piece> op = this.getPieceAt(bp);
		if (!op.isPresent()) return Optional.empty();
		else return Optional.of(op.get().getOwner());
	}
	
	/**
	 * @param p a piece object
	 * @param pc the piece's color
	 * @return a representation of the piece as describen in the FEN standard 
	 */
	private static String pieceToString(Piece p, PlayerColor pc) {
		String pieceString = p.toString();
		if(pc.getNum() == PlayerColor.WHITE.getNum())
			pieceString = pieceString.toUpperCase();
					
		return pieceString;
	}
	
	/**
	 * converts a board state in a string coherent with 
	 * the FEN standard 
	 * @param toMove - who has to move
	 * @param whiteCanShortCastle
	 * @param whiteCanLongCastle
	 * @param blackCanShortCastle
	 * @param blackCanLongCastle
	 * @param behindCaptureEnPassant
	 * @param numMoves
	 * @return
	 */
	public String stringifyStateForAI1v1(
			PlayerColor toMove, 
			boolean whiteCanShortCastle,
			boolean whiteCanLongCastle,
			boolean blackCanShortCastle,
			boolean blackCanLongCastle,
			BoardPosition behindCaptureEnPassant,
			int numMoves) {
		
		String[] fen = new String[6];
		
		
		// 0 > bianco > maiuscola
		// 1 > nero > minuscola
		
		// si scorre dai neri
		
		
		
		//CAMPO
		fen[0] = "";
		
		Collections.reverse(this.boardState);//perche si parte dai neri
		
		for(ArrayList<Cell> traversa : this.boardState) {
			int emptyCellFound = 0;

			for(Cell cell : traversa) {	
				
				Optional<Piece> p = cell.getPiece();
				if(p.isPresent()) {
					String pieceRappresentation = pieceToString(p.get(), p.get().getOwner());
					fen[0] = fen[0].concat(emptyCellFound == 0 ? "": String.valueOf(emptyCellFound)).concat(pieceRappresentation);
					emptyCellFound =0;
				}else {
					emptyCellFound++;
				}
				
			}
			
			fen[0] = fen[0].concat(emptyCellFound == 0 ? "": String.valueOf(emptyCellFound)).concat("/");
		}
		fen[0] = (new StringBuilder(fen[0])).deleteCharAt((fen[0].length()-1)).toString();
		Collections.reverse(this.boardState);

		///////////////////////////////////
		fen[1]= String.valueOf(toMove.getTxt().charAt(0));
		///////////////////////////////////
		fen[2]="";
		if(whiteCanShortCastle)fen[2]= fen[2].concat("K");
		if(whiteCanLongCastle)fen[2]= fen[2].concat("Q");
		if(blackCanShortCastle)fen[2]= fen[2].concat("k");
		if(blackCanLongCastle)fen[2]= fen[2].concat("q");
		if(fen[2].contentEquals(""))fen[2]= fen[2].concat("-");
		///////////////////////////////////
		if(behindCaptureEnPassant==null)fen[3]="-";
		else fen[3]=behindCaptureEnPassant.toString();
		///////////////////////////////////
		fen[4]="0";
		///////////////////////////////////
		int mosse = Math.floorDiv(2+numMoves, 2); // se loro contatno male
		fen[5]=String.valueOf( mosse )  ;
		
		
		return String.join(" ",fen);
	}
	
	/**
	 * feature moved in {@link logic.LogicGame}
	 */
	@Deprecated
	public boolean playersKingIsInCheck(PlayerColor kingColor) {
		ArrayList<BoardPosition> quadratiMinacciati = new ArrayList<>();
		BoardPosition kingPosition = findKing(kingColor).get().getPosition();
				
		//prendo dove si possono muovere tutti quelli del colore opposto<<<<<<< HEAD
		for(Piece pezzo : getListOfPieces()) {
			// se c'è il pezzo ed è nemico
			if( pezzo.getOwner() != kingColor) {
				// questa non va bene
				List<BoardPosition> possibleMoves = pezzo.getPossibleMoves(this);
				quadratiMinacciati.addAll(possibleMoves);
			}
		}
		for(ArrayList<Cell> riga : this.boardState) {
			for(Cell quadrato : riga) {
				Optional<Piece> pezzo = quadrato.getPiece();
				// se c'è il pezzo ed è nemico
				if(pezzo.isPresent() && pezzo.get().getOwner() != kingColor) {
					// questa non va bene
					List<BoardPosition> possibleMoves = pezzo.get().getPossibleMoves(this);
					quadratiMinacciati.addAll(possibleMoves);
				}else if(pezzo.isPresent() && pezzo.get() instanceof King && pezzo.get().getOwner() == kingColor){
					// gia che sto scorrendo la scacchiera
					// quando trovo il re da controllare mi prendo su la posizione
					kingPosition = pezzo.get().getPosition();
				}
			}
		}
		return quadratiMinacciati.contains(kingPosition);
	}
	
	/**
	 * feature moved in {@link logic.LogicGame#checkMate}
	 */
	@Deprecated
	public boolean playersKingIsInCheckMate(PlayerColor kingColor) {
		if (!playersKingIsInCheck(kingColor)) return false;
		//altrimenti so che il re è in scacco
		
		ArrayList<BoardPosition> quadratiMinacciati = new ArrayList<>();
		King king = findKing(kingColor).get();
		ArrayList<Piece> alleati = splitListOfPieces().get(kingColor);
		
		boolean qualcunoPuoFrapporsi = false;
		for(Piece pezzo : getListOfPieces()) {
			if(pezzo.getOwner() != kingColor) {
				List<BoardPosition> enemyPossibleMoves = pezzo.getPossibleMoves(this);
				
				for(Piece alleato : alleati) {
					if ( ! Collections.disjoint(
							enemyPossibleMoves, alleato.getPossibleMoves(this)
							)) qualcunoPuoFrapporsi = true;
					
				}					
				quadratiMinacciati.addAll(enemyPossibleMoves);
			}
		}
		if (qualcunoPuoFrapporsi) return false;

		
		return quadratiMinacciati.containsAll(king.getPossibleMoves(this));
	}
	
	/**
	 * checks if the match can go on
	 */
	public boolean matchIsInStaleMate() {
		// se qualcuno puo ancora dare scacco
		boolean minPiece = checkPlayersHasMinimumPieces(); 
		if(minPiece == false)return true;
		
		// e se i pezzi (in particolare il re) possono muoversi
		for(Piece pezzo : getListOfPieces()) {
			if( ! pezzo.getPossibleMoves(this).isEmpty())
				return true;
		}
		
		return false;
	}
	
	/**
	 * feature moved in {@link logic.LogicGame#DoMove}
	 */
	@Deprecated
	public void movePiece(BoardPosition from, BoardPosition to) throws Exception {
		
		Optional<Piece> pieceToMove = this.boardState.get(from.getX()).get(from.getY()).getPiece();
		if(!pieceToMove.isPresent()) throw new Exception("no piece in this position");
		
		movesDone++;
		pieceToMove.get().setPosition(to);
		this.boardState.get(from.getX()).get(from.getY()).setPiece(Optional.empty());
		this.boardState.get(to.getX()).get(to.getY()).setPiece(pieceToMove);
		
	}
	
	
	/**
	 * converts the ArrayList of ArrayList of Cell in a bi-dimensional array
	 */
	public Cell[][] getCellMatrix(){
		int sizeX=boardState.size();
		int sizeY=boardState.get(0).size();
		Cell ret[][]=new Cell[sizeX][sizeY];
		for(int i=0; i<sizeX; i++) {
			for(int j=0; j<sizeY; j++) {
				ret[i][j]=boardState.get(i).get(j);
			}
		}
		return ret;
	}
	
	/**
	 * @returns a whore column
	 */
	public ArrayList<Cell> getColumn(int col) {
		ArrayList<Cell> colonna = new ArrayList<>();
		for(List<Cell> riga : this.boardState) {
			colonna.add(riga.get(col));
		}
		return colonna;
	}
	
	/**
	 * @return a whole row
	 */
	public ArrayList<Cell> getRow(int row) {
		return this.boardState.get(row);
	}
	
	
	/**
	 * @return returns the player's king
	 */
	private Optional<King> findKing(PlayerColor kingColor) {
		for(Piece pezzo : getListOfPieces()) {
			if(	pezzo instanceof King && pezzo.getOwner() == kingColor)
				return Optional.of((King) pezzo);					
		}
		return Optional.empty();
	}
	
	
	/**
	 * @return if (at least) a player has enough piece to checkmate
	 */
	private boolean checkPlayersHasMinimumPieces() {
		Map<PlayerColor, ArrayList<Piece>> piecesSplitted = splitListOfPieces();
		for(PlayerColor pc : PlayerColor.values()) {
			// significa che un colore ha ancora abbastanza pezzi per  
			// dare scacco a tutti gli altri quindi ritorno un vero
			if (playersHasMinimumPiecesSet(piecesSplitted.get(pc))){
				return true;
			}
		}
		return false;
	}
	

	/**
	 * instantiates parameter as a shortcut for board's dimensions
	 */
	private void setColumnsAndRows() {
		this.numRows = this.boardState.size();
		this.numColumns = this.boardState.get(0).size();
	}
	

	/**
	 * just a giant method the checks if a player's set of piece
	 * is enough to check mate
	 * @param the piece list
	 */
	private boolean playersHasMinimumPiecesSet(ArrayList<Piece> playerPieceList) {
		// se c'� uno di questo set nei pezzi del user allora si puo andare avanti
		ArrayList<Piece> minPieceList1 = new ArrayList<Piece>() {
			private static final long serialVersionUID = 1L;
		{
			add( new King());
			add( new Knight());
			add( new Bishop());
		} };
		
		ArrayList<Piece> minPieceList2 = new ArrayList<Piece>() { 
			private static final long serialVersionUID = 1L;
		{
			add( new King());
			add( new Bishop());
			add( new Bishop());
		} };
		
		ArrayList<Piece> minPieceList3 = new ArrayList<Piece>() {
			private static final long serialVersionUID = 1L;
		{
			add( new King());
			add( new Rook());
		} };
			
		ArrayList<Piece> minPieceList4 = new ArrayList<Piece>() {
			private static final long serialVersionUID = 1L;
		{
			add( new King());
			add( new Queen());
		} };
			
		ArrayList<Piece> minPieceList5 = new ArrayList<Piece>() { 
			private static final long serialVersionUID = 1L;
		{
			add( new King());
			add( new Knight());
			add( new Knight());
			add( new Knight());
		} };
		
		//la lista delle liste
		ArrayList<ArrayList<Piece>> minPieceLists = new ArrayList<ArrayList<Piece>>() {
			private static final long serialVersionUID = 1L;
			{
				add(minPieceList1);
				add(minPieceList2);
				add(minPieceList3);
				add(minPieceList4);
				add(minPieceList5);
			}
		};
		
		for(ArrayList<Piece> minPieceList : minPieceLists)
			if (playerPieceList.containsAll(minPieceList))
				// se ha anche solo uno dei set mi basta
				return true;
			

		return false;
	}
	
	
	
	/**
	 * @return the whole set of pieces on the board 
	 */
	private ArrayList<Piece> getListOfPieces(){
		ArrayList<Piece> pieceList = new ArrayList<Piece>();
		for(ArrayList<Cell> r : this.boardState) {
			for(Cell c : r) {
				//scorro tutti i pezzi
				if(c.getPiece().isPresent())
					pieceList.add(c.getPiece().get());
			}
		}
		return pieceList;
	}


	
	// divide i pezzi sulla scacchiera per colore
	private Map<PlayerColor, ArrayList<Piece>> splitListOfPieces(){
		//faccio una mappa con tutti i giocatori e una lista di pezzi per giocatore
		Map<PlayerColor, ArrayList<Piece>> pieces = new HashMap<>();
		for(PlayerColor pc : PlayerColor.values()) {
			pieces.put(pc, new ArrayList<Piece>());
		}
		
		//scorro tutti i pezzi, e in mase al colore lo metto nella sua key
		ArrayList<Piece> lp = getListOfPieces();
		for(Piece p : lp) {
			pieces.get(p.getOwner()).add(p);		
		}
		
		return pieces;
	}

	
	/**
	 * converts the board state in the format
	 * described in {@link board.BoardSchema}
	 * @return the formatted string
	 */
	private String getStringification4v4() {
		String stringification = "";
		for(ArrayList<Cell> r : this.boardState) {
			for(Cell c : r) {
				String pieceString = null;
				if(!c.getPiece().isPresent()) {
					pieceString = "--";
				}
				else {
					String l = c.getPiece().get().toString().toUpperCase() ;
					int n = c.getPiece().get().getOwner().getNum();
					pieceString= l+String.valueOf(n);
				}
				stringification = stringification.concat(pieceString);
			}
			stringification = stringification.concat("/");
		}
		return stringification;
	}
	
	
	/**
	 * builds a copy of the state of the board
	 * the copy isn't done referencing the many objects but copying their values
	 * @return
	 * @throws Exception - the same exceptions raised by {@link board.BoardSchema#buildBoard}
	 */
	public ArrayList<ArrayList<Cell>> copyStateByValue() throws Exception {
		ArrayList<ArrayList<Cell>> copiedBoard = BoardSchema.buildBoard(this.getStringification4v4());
		for (int i=0; i<copiedBoard.size(); i++) {
			for (int j=0; j< copiedBoard.get(i).size(); j++) {
				Cell oldCell=this.boardState.get(i).get(j);
				Cell newCell=copiedBoard.get(i).get(j);
				if (oldCell.getPiece().isPresent() && oldCell.getPiece().get() instanceof King  && !((King)oldCell.getPiece().get()).getIsFirst()) {
					((King)newCell.getPiece().get()).markIsFirst();
				}
			}
		}
		return copiedBoard;
	}
	
	

	/**
	 * various getters and setters
	 * @return
	 */
	public Integer getNumColumns() {
		return numColumns;
	}

	public Integer getNumRows() {
		return numRows;
	}

	public ArrayList<ArrayList<Cell>> getBoardState() {
		return boardState;
	}

	public void setBoard(ArrayList<ArrayList<Cell>> newChessBoard) {
		this.boardState = newChessBoard;
		this.setColumnsAndRows();
	}
	





}
