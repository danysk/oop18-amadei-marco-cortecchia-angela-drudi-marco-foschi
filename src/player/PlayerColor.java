package player;

public enum PlayerColor {

	WHITE(0, "white"),
	BLACK(1, "black");
	
	private int num;
	private String txt;

	private PlayerColor(int num, String txt) {
		this.num = num;
		this.txt = txt;
	}

	public int getNum() {
		return num;
	}

	public String getTxt() {
		return txt;
	}
}