package test;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.Date;

import org.junit.jupiter.api.Test;
import logic.MatchScore;
import profile.HistoryImpl;
import profile.PlayerImpl;

class TestHistoryImpl {


	@Test
	void test() throws IOException {
		HistoryImpl history = new HistoryImpl();
		
		history.addMatch(new PlayerImpl(new MatchScore("x", 1, "y", 2, currentDate())));
		history.addMatch(new PlayerImpl(new MatchScore("t", 4, "z", 3, currentDate())));
		history.serialize();	
		
		history.matches.clear();
		try {
			history=HistoryImpl.deserialize();
			if (history.matches.size()>0)
				System.out.println(history.matches.get(0).getWinner()+" <- uploaded at least 1 match");
			else
				System.out.println("There are no matches!");
			assertTrue(history.matches.size()>0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void test2() throws IOException, ClassNotFoundException {
		HistoryImpl hist = HistoryImpl.deserialize();
		hist.toString();
		System.out.println(">" + hist.matches.get(0).getWinner() + " is the winner with " + hist.matches.get(0).getWinnerScore() + " points!"
				+ "\n\tDate of the game: " + hist.matches.get(0).getDate());
		System.out.println(">" + hist.matches.get(1).getWinner() + " is the winner with " + hist.matches.get(1).getWinnerScore() + " points!"
				+ "\n\tDate of the game: " + hist.matches.get(1).getDate());
	}
	
	@Test
	void test3() throws IOException, ClassNotFoundException,IndexOutOfBoundsException{
		HistoryImpl hist = HistoryImpl.deserialize();
		hist.deleteHistory();
		hist.matches.toString();
		assertEquals(0, hist.matches.size());
	}
	
	final Date currentDate() {
		Date currDate = new Date();
		return currDate;
	}
}