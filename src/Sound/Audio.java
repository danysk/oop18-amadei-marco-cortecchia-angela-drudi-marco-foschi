package Sound;
import java.io.File; 
import java.io.IOException; 
  
import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.Clip; 
import javax.sound.sampled.LineUnavailableException; 
import javax.sound.sampled.UnsupportedAudioFileException;

import settings.CrossPlatformResources;
import settings.GlobalSettingsImpl; 

/**
 * Foschi Riccardo
 * 
 * Classe utilizzata per utilizzare file Audio
 *
 */
public class Audio  {

	
	String path;	
	Clip clip;        
    AudioInputStream audioInputStream; 

	
	public Audio(String path) throws UnsupportedAudioFileException, IOException, LineUnavailableException 
	{
		
		this.path=path;
		
		//create an AudioStream
		audioInputStream = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
		// create clip reference 
        clip = AudioSystem.getClip();          
        // open audioInputStream to the clip 
        clip.open(audioInputStream);           
        clip.loop(0);
	}
	
	public static void playSound(String path){

		if (GlobalSettingsImpl.isAudioEnabled)
		{
			try {
				File f = CrossPlatformResources.getResourceFile(path);
				playSoundFromFile(f);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * Metodo far partire audio 
	 * 
	 * @param path path del file audio 
	 * @throws Exception
	 */
	
	static void playSoundFromFile(File file) throws Exception{

		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file.getAbsoluteFile());
		// create clip reference 
        Clip clip = AudioSystem.getClip();          
        // open audioInputStream to the clip 
        clip.open(audioInputStream);           
        clip.loop(0);
        clip.start(); 
	}
	
	/**
	 * Metodo per far partire audio
	 */
	 
    public void play()  
    { 
        
		if (GlobalSettingsImpl.isAudioEnabled)
        clip.start(); 
    } 
	/**
	 * Metodo per stop Audio
	 */
    public void stop() throws UnsupportedAudioFileException,IOException, LineUnavailableException  
    { 
        
        clip.stop(); 
        clip.close(); 
    } 
    
    
	
}