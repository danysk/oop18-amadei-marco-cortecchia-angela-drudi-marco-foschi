package logic;

import java.io.Serializable;
/**
 * Foschi Riccardo
 * 
 * Classe che ha lo scopo di tenere il punteggio finale di una partita
 *
 */
import java.util.Date;

public class MatchScore implements Serializable {

	private static final long serialVersionUID = 1L;
	String player1;
	String player2;
	private final int score1;
	private final int score2;
	Date date = new Date();
	
	public MatchScore(String p1,int score1,String p2,int score2, Date date) {
		this.player1=p1;
		this.score1=score1;
		this.player2=p2;
		this.score2=score2;
		this.date = date;
	}
	/**
	 * Ritorna il punteggio finale del giocatore 1
	 * 
	 */
	public int finalScore1() {
		return score1;
	}
	/**
	 * 
	 * Ritorna il punteggio finale del giocatore 2
	 * 
	 */
	public int finalScore2() {
		return score2;
	}
	
	/**
	 * Ritorna il player 1
	 * 
	 */
	public String player1() {
		return player1;
	}
	/**
	 * Ritorna il giocatore 2
	 * 
	 */
	public String player2() {
		return player2;
	}

	public Date getDate() {
		return date;
	}
}