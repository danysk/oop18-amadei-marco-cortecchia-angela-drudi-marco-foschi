package logic;
/*
 * 
 * 
 * Questa classe serve a tenere traccia in fase di creazione della partita le informazioni dei player 
 * che hanno joinato la partita
 */

public class PlayerType {
	public boolean isAi;
	public String nickname;
	
	public PlayerType(boolean isAi, String nickname)
	{
		this.isAi=isAi;
		this.nickname=nickname;
	}
}
