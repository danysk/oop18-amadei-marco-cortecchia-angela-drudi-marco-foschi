package logic;

import java.util.ArrayList;

import board.BoardPosition;
import piece.Cell;
import player.PlayerColor;

public interface StrategyPlayerLogic {
	
	
	
	boolean chess(ArrayList<ArrayList<Cell>> boardCopy,PlayerColor myColor);//se sono sotto scacco
	
	boolean overBoard(BoardPosition myPosition,BoardPosition nextPosition);//se la mossa che voglio fare vado fuori scacchiera/
	
	boolean myTurn(Cell myCell);// controllo se Ã¨ il mio turno
	
	//controllo se sposto sposto questo pezzo sono sotto scacco
	boolean ifImoveIamInCheck(BoardPosition myPosition);

		
	//List<Piece> getEating();//ritorna la lista di pezzi mangiati
	
}