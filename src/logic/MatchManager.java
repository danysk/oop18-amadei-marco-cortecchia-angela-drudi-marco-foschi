package logic;

/**
 * 
 * @author Marco Amadei
 * 
 * Classe che gestisce cosa succede all'inizio ed al termine di una partita e ne tiene informazioni relative alle impostazioni di partita
 *
 */
import java.io.IOException;

import board.GenericBoard;
import board.StandardBoard;
import gui.BoardGUI;
import logic.LogicGame;
import logic.PlayerType;
import player.PlayerColor;
import profile.HistoryImpl;
import profile.PlayerImpl;
import settings.MatchSettings.Difficulty;
import settings.MatchSettings.GameMode;
import settings.MatchSettings.GameTime;

public class MatchManager {
	private LogicGame gameLogic;
	private BoardGUI currentBoardGUI;
	public String resourcesPath="standard";
	GameTime timeSettings;
	public PlayerType players[];
	public Difficulty difficulty;
	 
	int maxMatches=1;
	int currentMatch=0;
	
	public MatchManager(Difficulty diff,GameMode gameMode,String skinPath, GameTime timeSettings, PlayerType players[], int matchNumber) throws Exception {
		difficulty=diff;
		//set up stuff
		this.resourcesPath=skinPath;
		this.timeSettings=timeSettings;
		this.players=players;
		this.maxMatches=matchNumber;
		if (this.maxMatches<=0)	{
			this.maxMatches=1;
			System.out.println("WARNING: You cant make less then 1 match!");
		}
		else if (this.maxMatches>10){
			this.maxMatches=10;
			System.out.println("WARNING: You cant make more then 10 matches!");
		}
		this.currentMatch=0;

		NextMatch();//inizia con la prima partita
	}

    /**
     * @return Current active GUI for the board..
     */
	public BoardGUI getCurrentBoardGUI(){
		return currentBoardGUI;
	}

    /**
     * @return Current active chess board.
     */
	public GenericBoard getBoard(){
		return gameLogic.getChessBoard();
	}

    /**
     * @return Current active game logic.
     */
	public LogicGame getGameLogic()	{
		return gameLogic;
	}


    /**
     * To call when a game ends.
     * @param winner Color of the winner.
     * @param score Scores of the players.
     * @param time Time from the start of the match.
     */
    public void onGameEnded(PlayerColor winner, MatchScore score, int time)  {
    	if (score.finalScore1()>0 || score.finalScore2()>0) {
    		try {
				saveMatchData(score);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
    	}
    	if (winner==null){
        	currentBoardGUI.printEndedMatch(" - ",0,currentMatch>=maxMatches);
    	}
    	else 	{
    		if (score.finalScore1()>score.finalScore2())
    	    	currentBoardGUI.printEndedMatch(score.player1, score.finalScore1(),currentMatch>=maxMatches);
    		else
    	    	currentBoardGUI.printEndedMatch(score.player2, score.finalScore2(),currentMatch>=maxMatches);
    	}
    	//apri la gui che mostra i risultati
    }

    /**
     * @return A string representation of the current match number over all matches to do.
     */
    public String getMatchNumberInfo(){
    	return "#"+currentMatch+"/"+maxMatches;
    }

    /**
     * Start next match (or back to menu if this was last match).
     */
    public void NextMatch() {
    	currentMatch++;
    	if (currentMatch>maxMatches){
    		//era l'ultima partita, non far partire nessun match.
    	}
    	else{
    		currentBoardGUI=new BoardGUI(this);
    		gameLogic=new LogicGame(players.length,this);
    		try {
				gameLogic.setChessBoard(new StandardBoard());
				gameLogic.StartGame();
			} catch (Exception e) {
				e.printStackTrace();
			}

    	}
    }

    /**
     * Save a new score.
     * @param score The score to save.
     */
    private void saveMatchData(MatchScore score) throws ClassNotFoundException, IOException
    {
		HistoryImpl rank = HistoryImpl.deserialize();
		PlayerImpl playerScore=new PlayerImpl(score);
		playerScore.diff=difficulty;
		rank.addMatch(playerScore);
    	System.out.println("Sto salvando "+rank.matches.size()+" matches");
		rank.serialize();
		
		System.out.println(HistoryImpl.deserialize().matches.size()+" <- riletto");
    }
}
