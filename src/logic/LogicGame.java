package logic;

import java.net.MalformedURLException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
 
import ai.AIInterface;
import ai.Move;
import ai.exceptions.StockfishInitException;
import board.BoardPosition;
import board.GenericBoard;
import logic.MatchManager;
import piece.*;
import player.PlayerColor;

/**
 * 
 * Riccardo Foschi
 *  
 *  Gestisce :
 *  - Cambio turno
 *  - Scacco matto
 *  - Inizio partita
 *  - Fine partita 
 *  - Punteggio Finale giocatori
 *  - Input per fare spostamento (sia che sia giocatore che IA)
 *
 */
public class LogicGame implements StrategyLogicGame{
	
	private int numberplayer;//numero i giocatori con cui voglio fare la partita	
	private int turno=0;
	public  Set<Piece>eat=new HashSet<>();//sono i pezzi mangiati di tutti i giocatori		
	public  Map<Piece,List<BoardPosition>>pieceDoMove=new HashMap<>();//sono tutte le mosse possibili da ogni pezzo
	
	public GenericBoard chessBoard;//la mia scacchiera	
	PlayerLogic logic;
	MatchManager matchManager;	
	public boolean gameEnded=false;
	Time timer;
	
	
	
	public LogicGame(int numeroGiocatoriRichiesti, MatchManager manager) {
		
		if (numeroGiocatoriRichiesti<=1) {			
			//mi deve dare un eccezione perchè¨ non si puo avere solo un giocatore 
			System.out.println("Error, min 2");
			throw new IllegalArgumentException();
		}
		this.numberplayer=numeroGiocatoriRichiesti;
		logic=new PlayerLogic(this);
		this.matchManager=manager;

	}
	
	/**
	 * Cambia effettivamente Il turno e resetta il timer
	 */
	@Override
	public void changeRounders() {
		turno++;
		resetTimer();
		
	}
	
	/**
	 * Resetta il timer
	 * 
	 */
	public void resetTimer(){
		if (timer!=null)
			timer.setIgnoreTimer();
		timer=new Time(matchManager.timeSettings,this);
		new Thread(timer).start();
		
	}
	
	/**
	 * Aggiorna il tempo nella gui
	 * 
	 */
	public void OnRemainingTimeUpdated(int time)	{
		matchManager.getCurrentBoardGUI().updateRemainingTime(time);
	}

	/**
	 *  Ritorna il colore del giocatore che NON è il giocaotre corrente
	 * 
	 */
	private PlayerColor getOpponent() {
		if(getCurrentPlayer()==PlayerColor.WHITE.getNum())
			return PlayerColor.BLACK;
		else
			return PlayerColor.WHITE;
					
	}
	/**
	 *  Ritorna true se è scacco matto 
	 *  
	 */
	
	@Override
	public boolean checkMate(){
		if (chessBoard==null) 
			return false;
		try {
			if (!logic.chess(chessBoard.copyStateByValue(), getCurrentPlayerColor()))//se non � in scacco allora non � in scacco matto
				return false;

			ArrayList<ArrayList<Cell>> boardCopy= chessBoard.copyStateByValue();
			for(ArrayList<Cell>a:boardCopy) {
				for(Cell b:a ) //per ogni cella
				{
					if(b.getPiece().isPresent() && b.getPiece().get().getOwner()==getCurrentPlayerColor())//se c� una pedina ed � del giocatore a cui tocca
					{
						for (BoardPosition pos: b.getPiece().get().getPossibleMoves(new GenericBoard(boardCopy)))//per ogni mossa possibile della pedina esaminata
						{
							if (!logic.check(b.getPiece().get().getPosition(), pos, getCurrentPlayerColor()))//se questa mossa mi salverebbe
								return false;//non sono in scacco matto
						}
					}
				}
			}
			return true;//nessuna mossa che mi puo salvare trovata allora sono in scacco matto
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
	
	/**
	 * Ritorna il risultato finale
	 * 
	 */
	
	private MatchScore getGameScore()
	{
		return new MatchScore(matchManager.players[0].nickname, getScore(PlayerColor.values()[0]), matchManager.players[1].nickname, getScore(PlayerColor.values()[1]),new Date());
		
	}
	
	private int getGameTime()
	{
		return 0;
	}
	
	/**
	 *  Fa finire la partita nel match manager
	 * 
	 */
	public void endGame(PlayerColor winner)
	{
		if (!gameEnded)
		{
			gameEnded=true;
			matchManager.onGameEnded(winner, getGameScore(), getGameTime());
		}
	}
	
	/**
	 * Ritorna il colore del giocatore corrente
	 */
	@Override
	public PlayerColor getRounder() {
		return getCurrentPlayerColor();
	}
	
	/**
	 * Ritorna il colore del giocatore vincitore
	 */	
	@Override
	public PlayerColor win() {
		 if(checkMate())
			 return getOpponent();
		 else return null;
	}
	
	/**
	 * Ritorna la scacchiera 
	 * 
	 */
	public GenericBoard getChessBoard() {
		return chessBoard;
	}
	
	/**
	 * 	Modifica la scacchiera
	 * 
	 */
	public void setChessBoard(GenericBoard chessBoard) {
		 this.chessBoard=chessBoard;
		updateListDoMove();
	}
	
	/**
	 * Starta la partita
	 */
	public void StartGame()
	{
		try {
			resetTimer();
			startTurn();
		} catch (MalformedURLException | StockfishInitException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ritorna il punteggio di un giocatore
	 */
	@Override
	public int getScore(PlayerColor color) {
		int score=0;
		for(Piece a:eat) {
			if(a.getOwner()!=color)
				score=score+a.getScore();
		}
		return score;		
	}
	
	/**
	 * 	Ritorna se il giocatore è un AI
	 * 
	 */
	public boolean isPlayerTurn()
	{
		return !matchManager.players[getCurrentPlayer()].isAi;
	}
	
	/**
	 * Dopo gli opportuni controlli, fa fare la mossa all' AI
	 * @throws MalformedURLException
	 * @throws StockfishInitException
	 * @throws InterruptedException
	 */
	public void startTurn() throws MalformedURLException, StockfishInitException, InterruptedException
	{	
		if (gameEnded)
			return;
		
		if(checkMate()) {
			endGame(win());
			return;
		}
		
		if (matchManager.getCurrentBoardGUI()!=null)
			matchManager.getCurrentBoardGUI().UpdateGUI();	
		
		if (matchManager.players[getCurrentPlayer()].isAi)
		{
			AiDoMove();
		}
	}
	
	/**
	 *  Fa lo spostamento con la AI
	 * @throws MalformedURLException
	 * @throws StockfishInitException
	 * @throws InterruptedException
	 */
	public void AiDoMove() throws MalformedURLException, StockfishInitException, InterruptedException
	{
		BoardPosition whiteKingPosition= logic.isKing(chessBoard.getBoardState(), PlayerColor.WHITE);
		BoardPosition blackKingPosition= logic.isKing(chessBoard.getBoardState(), PlayerColor.BLACK);
		boolean blackCanShortCastle=blackKingPosition!=null? ((King)chessBoard.getPieceAt(blackKingPosition).get()).canShortCastle(chessBoard):false;
		boolean blackCanLongCastle=blackKingPosition!=null? ((King)chessBoard.getPieceAt(blackKingPosition).get()).canLongCastle(chessBoard):false;
		boolean whiteCanShortCastle=blackKingPosition!=null? ((King)chessBoard.getPieceAt(whiteKingPosition).get()).canShortCastle(chessBoard):false;
		boolean whiteCanLongCastle=blackKingPosition!=null? ((King)chessBoard.getPieceAt(whiteKingPosition).get()).canLongCastle(chessBoard):false;
		
		AIInterface ai = new AIInterface();
		boolean showThinkingFrame=!matchManager.players[0].isAi || !matchManager.players[1].isAi;
		Move move =ai.calculateMove(chessBoard,getCurrentPlayerColor(), matchManager.difficulty, whiteCanShortCastle, whiteCanLongCastle, blackCanShortCastle, blackCanLongCastle, null,turno, showThinkingFrame);
		
		logic.registerEatAt(chessBoard.getBoardState(), move.getTo());
		logic.MovePiece(chessBoard.getBoardState(),move.getFrom(),move.getTo(),true);
		//se tocca ancora alla IA richiama questa

		if(checkMate()) {
			endGame(win());
			return;
		}


		changeRounders();
		//dopo aver cambiato il turno aggiorno le possibili mosse di tutti i giocatori
		pieceDoMove.clear();
		updateListDoMove();

		Thread.sleep(1000);

		startTurn();
	}
	
	/**
	 * Spostamento con giocatore fisico
	 */
	@Override
	public void DoMove(BoardPosition myPosition, BoardPosition nextPosition, boolean checkCanMoveThere) {
		boolean m=false;
		try {
			m=logic.move(myPosition, nextPosition,checkCanMoveThere);
			//se Ã¨ giusto mi cambia turno
			if(m) {
				if(checkMate()) {
					endGame(win());
					return;
				}

				changeRounders();
				//dopo aver cambiato il turno aggiorno le possibili mosse di tutti i giocatori
				pieceDoMove.clear();
				updateListDoMove();
				
				startTurn();
			}						
		} catch (Exception e) {
			System.out.println("DoMove had an error!");
			e.printStackTrace();
		}
		
	}	
	
	/**
	 * Aggiorna la lista 
	 */
	private void updateListDoMove() {
		for(Cell[] a:chessBoard.getCellMatrix()) {
			for(Cell b:a) {
				if(b.getPiece().isPresent())
					pieceDoMove.put(b.getPiece().get(),b.getPiece().get().getPossibleMoves(chessBoard));
			}
		}
		
	}
	
	
	public int getCurrentPlayer()
	{
		return turno%numberplayer;
	}
	
	public PlayerColor getCurrentPlayerColor()
	{
		return PlayerColor.values()[turno%numberplayer];
	}
	
	public int getCurrentTurn()
	{		
		return turno+1;
	}
}