package piece;

import java.util.Optional;

import board.BoardPosition;

public interface Cell {

	/**
	 * Returns a state of the cell
	 * @return a boolean value
	 */
	public boolean isValid();
	
	/**
	 * Set true the state of the cell
	 */
	public void enable();
	
	/**
	 * Set false the state of the cell
	 */
	public void disable();
	
	/**
	 * Returns a piece in a cell
	 * @return a Optional of piece if is present otherwise Optional.empty()
	 */
	public Optional<Piece> getPiece();
	
	/**
	 * Set a piece in a cell
	 * @param piece is a Optional<piece>
	 */
	public void setPiece(Optional<Piece> piece);
	
	/**
	 * Returns a board position of the cell
	 * @return a BoardPosition
	 */
	public BoardPosition getBoardPosition();
}
