package piece;

import java.util.List;

import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public abstract class AbstractPiece implements Piece{

	protected final PlayerColor owner;
	protected BoardPosition position;
	
	public AbstractPiece() {
		this.owner = null;
		this.position = null;
	}
	
	public AbstractPiece(final PlayerColor owner, final BoardPosition position) {
		this.owner = owner;
		this.position = position;
	}
	
	public PlayerColor getOwner() {
		return this.owner;
	}
	
	public BoardPosition getPosition() {
		return this.position;
	}
	
	public void setPosition(final BoardPosition position) {
		this.position = position;
	}
	
	public abstract List<BoardPosition> getPossibleMoves(GenericBoard board);
	
	public abstract String getIcon();
	
	public abstract int getScore();
	
	public abstract String toString();
}
