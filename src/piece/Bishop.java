package piece;

import java.util.ArrayList;
import java.util.List;
import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public class Bishop extends AbstractPiece{

	public Bishop() {
		super();
	}
	
	public Bishop(PlayerColor owner, BoardPosition position) {
		super(owner, position);
	}

	@Override
	public List<BoardPosition> getPossibleMoves(GenericBoard board) {
		List<BoardPosition> possibleMoves = new ArrayList<>();
		
		int x = this.position.getX();
		int y = this.position.getY();
		x++;
		y++;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x++;
			y++;	
		}
		
		x = position.getX();
		y = position.getY();
		x++;
		y--;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x++;
			y--;	
		}
		
		x = position.getX();
		y = position.getY();
		x--;
		y++;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x--;
			y++;	
		}
		
		x = position.getX();
		y = position.getY();
		x--;
		y--;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x--;
			y--;	
		}
		
		return possibleMoves;
	}

	@Override
	public String getIcon() {
		return "Bishop.png";
	}

	@Override
	public int getScore() {
		return 3;
	}
	
	@Override
	public String toString() {
		return "b";
	}
}
