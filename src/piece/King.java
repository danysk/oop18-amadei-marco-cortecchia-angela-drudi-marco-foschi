package piece;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public class King extends AbstractPiece{

	private boolean isFirst;
	
	public King() {
		super();
	}
	
	public King(PlayerColor owner, BoardPosition position) {
		super(owner, position);
		this.isFirst = true;
	}

	@Override
	public List<BoardPosition> getPossibleMoves(GenericBoard board) {
		List<BoardPosition> possibleMoves = new ArrayList<>();
		possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()));
		possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()));
		possibleMoves.add(new BoardPosition(position.getX(), position.getY()+1));
		possibleMoves.add(new BoardPosition(position.getX(), position.getY()-1));
		possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()-1));
		possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()+1));
		possibleMoves.add(new BoardPosition(position.getX()-1, position.getY()-1));
		possibleMoves.add(new BoardPosition(position.getX()+1, position.getY()+1));
		possibleMoves = possibleMoves.stream().filter(x -> x.isValidClassicBoardPositions())
							  .filter(x -> !board.getPieceColorAtPosition(x).isPresent() || !board.getPieceColorAtPosition(x).get().equals(this.owner))
							  .collect(Collectors.toList());
		

		if(this.canLongCastle(board)) 
			possibleMoves.add(new BoardPosition(position.getX(), position.getY()+2));
		if(this.canShortCastle(board))
			possibleMoves.add(new BoardPosition(position.getX(), position.getY()-2));
		
		return possibleMoves;
	}

	@Override
	public String getIcon() {
		return "King.png";
	}

	@Override
	public int getScore() {
		return 10;
	}
	
	@Override
	public String toString() {
		return "k";
	}
	
	public boolean getIsFirst() {
		return this.isFirst;
	}
	
	public void markIsFirst() {
		this.isFirst = false;
	}
	
	/**
	 * Check that there are the conditions to do long castle
	 * @param board
	 * @return true if is possible, otherwise false
	 */
	public boolean canLongCastle(GenericBoard board) {

		if(!new BoardPosition(this.position.getX(), this.position.getY()+3).isValidClassicBoardPositions())
			return false;
		
		if (this.isFirst)
		{
			Optional<Piece> rook;
			rook = board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()+3));
			
			if(rook.isPresent()) {
				if(rook.get() instanceof Rook) {
					if(((Rook) rook.get()).getIsFirst()){
						if(this.isFirst) {
							if(!board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()+1)).isPresent() && 
									!board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()+2)).isPresent())
								return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Check that there are the conditions to do short castle
	 * @param board
	 * @return true if is possible, otherwise false
	 */
	public boolean canShortCastle(GenericBoard board) {

		if(!new BoardPosition(this.position.getX(), this.position.getY()-4).isValidClassicBoardPositions())
			return false;
		
		if (this.isFirst)
		{
			Optional<Piece> rook;
			rook = board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()-4));
			
			if(rook.isPresent()) {
				if(rook.get() instanceof Rook) {
					if(((Rook) rook.get()).getIsFirst()){
						if(this.isFirst) {
							if(!board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()-1)).isPresent() && 
									!board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()-2)).isPresent() && 
									!board.getPieceAt(new BoardPosition(this.position.getX(), this.position.getY()-3)).isPresent())
								return true;
						}
					}
				}
			}
		}
		return false;
	}
}
