package piece;

import java.util.Optional;

import board.BoardPosition;

public class CellImpl implements Cell {

	private boolean state;
	private Optional<Piece> piece;
	private final BoardPosition position;
	
	public CellImpl(final boolean state, final Piece piece, final BoardPosition position) {
		if(piece.getOwner().equals(null) || piece.getPosition().equals(null))
			throw new IllegalArgumentException();
		this.state = state;
		this.piece = Optional.of(piece);
		this.position = position;
	}
	
	public CellImpl(final boolean state, final BoardPosition position) {
		this.state = state;
		this.piece = Optional.empty();
		this.position = position;
	}
	
	@Override
	public boolean isValid() {
		return this.state;
	}

	@Override
	public void enable() {
		this.state = true;
	}

	@Override
	public void disable() {
		this.state = false;
	}

	@Override
	public Optional<Piece> getPiece() {
		return this.piece;
	}

	@Override
	public void setPiece(Optional<Piece> piece) {
		this.piece = piece;
	}

	@Override
	public BoardPosition getBoardPosition() {
		return this.position;
	}
}
