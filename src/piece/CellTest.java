package piece;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import board.BoardPosition;

class CellTest {

	private BoardPosition position;
	private Cell cell;
	private Piece piece;
	
	@Test
	void testStateCell() {
		this.position = new BoardPosition(2, 2);
		this.cell = new CellImpl(false, position);
		assertFalse(this.cell.isValid());
		this.cell.enable();;
		assertTrue(this.cell.isValid());
		this.cell.disable();
		assertFalse(this.cell.isValid());
	}
	
	@Test
	void testPieceInCell() {
		this.position = new BoardPosition(2, 2);
		this.piece = new King();
		this.cell = new CellImpl(false, position);
		assertEquals(Optional.empty(), this.cell.getPiece());
		this.cell.setPiece(Optional.of(this.piece));
		assertEquals(Optional.of(this.piece), this.cell.getPiece());
	}

	@Test
	void testBoardPositionsOfCell() {
		this.position = new BoardPosition(2, 2);
		this.cell = new CellImpl(false, position);
		assertEquals(this.position, this.cell.getBoardPosition());
	}
	
}
