package gui;

/**
 * 
 * @author Marco Amadei
 * 
 * GUI per la creazione di una nuova partita
 *
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import Sound.Audio;
import logic.MatchManager;
import logic.PlayerType;
import settings.MatchSettings;
import settings.MatchSettings.GameMode;
import settings.MatchSettings.GameTime;
import settings.Translations;
import settings.MatchSettings.Difficulty;

public class NewGameConfigGUI extends JFrame
{
	private static final long serialVersionUID = 1L;

    private JButton newGame = new JButton("New Game");
    private JButton close = new JButton("Back");
    private JComboBox<String> difficultyDD= new JComboBox<>();
    private JComboBox<String> skinsDD= new JComboBox<>();
    private JComboBox<String> timeDD= new JComboBox<>();
    private JTextField playerName1=new JTextField();
    private JTextField playerName2=new JTextField();
    private JCheckBox isPlayer1AI=new JCheckBox();
    private JCheckBox isPlayer2AI=new JCheckBox();
    private JSpinner matchNumber = new JSpinner();
    
    public NewGameConfigGUI(){
        super("Java Chess");//chiama costruttore di base
   
        
        //SETUP MAIN PANEL
        JPanel MainPanel = new JPanel(new GridBagLayout());
        MainPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "<html><font color='white'>"+Translations.getTranslation("newgame")));
        GridBagConstraints constraints = new GridBagConstraints();
        MainPanel.setOpaque(true);
        MainPanel.setBackground(Color.GRAY);
        MainPanel.setForeground(Color.BLACK);

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(10, 10, 10, 10);

        //DIFFICULTY
        constraints.gridx = 0;
        constraints.gridy = 1;
        JLabel label1=new JLabel("Difficulty");
        new TextToTranslationJLabel(label1, "difficulty");
        label1.setForeground(Color.white);
        MainPanel.add(label1,constraints);
        //new TextToTranslationJLabel(label1,"difficulty");
        //SETUP DIFFICULTY SCROLL DOWN
        for (MatchSettings.Difficulty diff : MatchSettings.Difficulty.values()) 
        	difficultyDD.addItem(diff.toString());
        ActionListener al = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        };
        difficultyDD.addActionListener(al);
        difficultyDD.setBackground(Color.WHITE);
        difficultyDD.setForeground(Color.BLACK);
        constraints.gridx = 1;
        constraints.gridy = 1;
        MainPanel.add(difficultyDD,constraints);


        //SKINS
        constraints.gridx = 0;
        constraints.gridy = 2;
        JLabel label3=new JLabel("Skin pack");
        new TextToTranslationJLabel(label3, "skinpack");
        label3.setForeground(Color.white);
        MainPanel.add(label3,constraints);
        //new TextToTranslationJLabel(label3,"skinpack");
        //SETUP SKINS SCROLL DOWN
        String[] folders=SkinPackFinder.getSkinFolders();
        for (int i=0; i<folders.length; i++) 
        	skinsDD.addItem(folders[i]);
        ActionListener al3 = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        };
        skinsDD.addActionListener(al3);
        skinsDD.setBackground(Color.WHITE);
        skinsDD.setForeground(Color.BLACK);
        constraints.gridx = 1;
        constraints.gridy = 2;
        MainPanel.add(skinsDD,constraints);


        //TIME
        constraints.gridx = 0;
        constraints.gridy = 3;
        JLabel label4=new JLabel("Time settings");
        new TextToTranslationJLabel(label4, "timesettings");
        label4.setForeground(Color.white);
        MainPanel.add(label4,constraints);
        //new TextToTranslationJLabel(label4,"timesettings");
        //SETUP TIME SCROLL DOWN
        for (MatchSettings.GameTime diff : MatchSettings.GameTime.values()) 
        	timeDD.addItem(diff.toString());
        ActionListener al4 = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        };
        timeDD.addActionListener(al4);
        timeDD.setBackground(Color.WHITE);
        timeDD.setForeground(Color.BLACK);
        constraints.gridx = 1;
        constraints.gridy = 3;
        MainPanel.add(timeDD,constraints);
        
        JLabel label;
        Dimension dim=new Dimension(100,20);
        
        //>>>player 1
        constraints.gridx = 0;
        constraints.gridy = 4;
        label=new JLabel("Player 1 Name");
        new TextToTranslationJLabel(label, "player1name");
        label.setForeground(Color.white);
        MainPanel.add(label,constraints);

        constraints.gridx = 1;
        constraints.gridy = 4;
        playerName1=new JTextField();
        playerName1.setText("Game Host");
        playerName1.setSize(dim);
        //playerName1.setForeground(Color.white);
        MainPanel.add(playerName1,constraints);
        
        constraints.gridx = 2;
        constraints.gridy = 4;
        isPlayer1AI=new JCheckBox("Is AI");
        new TextTotranslationJCheckBox(isPlayer1AI, "isai");
        isPlayer1AI.setSelected(false);
        //isPlayer1AI.setForeground(Color.white);
        MainPanel.add(isPlayer1AI,constraints);
        
        //>>>player 2
        constraints.gridx = 0;
        constraints.gridy = 5;
        label=new JLabel("Player 2 Name");
        new TextToTranslationJLabel(label, "player2name");
        label.setForeground(Color.white);
        MainPanel.add(label,constraints);

        constraints.gridx = 1;
        constraints.gridy = 5;
        playerName2=new JTextField();
        playerName2.setSize(dim);
        playerName2.setText("Opponent");
        //playerName2.setForeground(Color.white);
        MainPanel.add(playerName2,constraints);
        
        constraints.gridx = 2;
        constraints.gridy = 5;
        isPlayer2AI=new JCheckBox("Is AI");
        new TextTotranslationJCheckBox(isPlayer2AI, "isai");
        isPlayer2AI.setSelected(true);
        //isPlayer2AI.setForeground(Color.white);
        MainPanel.add(isPlayer2AI,constraints);
        
        boolean isOnMAC=System.getProperty("os.name").toLowerCase().indexOf("mac") >= 0;        
        //disabilita AI su mac
        if (isOnMAC){
        	isPlayer1AI.setEnabled(false);
        	isPlayer1AI.setSelected(false);
        	isPlayer2AI.setEnabled(false);
        	isPlayer2AI.setSelected(false);
        	isPlayer1AI.setVisible(false);
        	isPlayer2AI.setVisible(false);
        }
        
        //matches
        constraints.gridx = 0;
        constraints.gridy = 6;
        label=new JLabel("Matches");
        new TextToTranslationJLabel(label, "matches");
        label.setForeground(Color.white);
        MainPanel.add(label,constraints);

        constraints.gridx = 1;
        constraints.gridy = 6;
        matchNumber=new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
        matchNumber.setForeground(Color.white);
        MainPanel.add(matchNumber,constraints);
        
        

        //new game button
        newGame.addActionListener(e -> {try {
			StartNewGame();
		} catch (Exception e1) {
			e1.printStackTrace();
		}});
        new TextToTranslationJButton(newGame,"newgame");
        constraints.gridx = 1;
        constraints.gridy = 7;
        newGame.setBackground(Color.WHITE);
        newGame.setForeground(Color.BLACK);
        MainPanel.add(newGame,constraints);
        
        
        //close button
        close.addActionListener(e -> {Back();});
        new TextToTranslationJButton(close,"back");
        constraints.gridx = 0;
        constraints.gridy = 7;
        close.setBackground(Color.WHITE);
        close.setForeground(Color.BLACK);
        MainPanel.add(close,constraints);
        
        //FRAME OPTIONS
        add(MainPanel);
        setSize(800, 600);
        pack();
        setVisible(true);
    }

    /**
     * Start a new game from the given settings.
     */
    private void StartNewGame() throws Exception
    {
		Audio.playSound("/clips/button_01.wav");
				
    	PlayerType players[]=new PlayerType[] {new PlayerType(isPlayer1AI.isSelected(),playerName1.getText()),new PlayerType(isPlayer2AI.isSelected(),playerName2.getText())};

		if (players[0].isAi && players[1].isAi)
		{
			new WarningGUI(Translations.getTranslation("no2ai"));
			return;
		}
    	
    	Difficulty diff=Difficulty.values()[difficultyDD.getSelectedIndex()];
    	GameTime gtime=GameTime.values()[timeDD.getSelectedIndex()];
    	int numOfMatches=(int)matchNumber.getValue();
    	String skinDirectory=SkinPackFinder.getSkinFolders()[skinsDD.getSelectedIndex()];

    	new MatchManager(diff,GameMode.STANDARD,skinDirectory, gtime,players,numOfMatches);
    	Close();
    }

    /**
     * Close this panel and play a back sound.
     */
    private void Back()
    {
		Audio.playSound("/clips/button_01.wav");
    	Close();
    }
    /**
     * Close this panel.
     */
    private void Close()
    {
    	setVisible(false); //you can't see me!
    }
}