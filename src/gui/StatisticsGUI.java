package gui;
/**
 * 
 * @author Angela Cortecchia
 *
 */
import java.awt.Color;
import profile.HistoryImpl;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import settings.Translations;

public class StatisticsGUI extends JFrame{

	private static final long serialVersionUID = 1L;

	private JButton close = new JButton("Close");
	private JButton clear = new JButton("Clear");
	private JLabel title = new JLabel("\"<html><font color='yellow'>Statistics</font><font color='red'></font>\"");
	private JButton showHistory = new JButton("history");
	JPanel MainPanel;
	
	public StatisticsGUI() {
		super("java chess"); //chiama costruttore di base
		
		//MAIN PANEL SETUP
		MainPanel = new JPanel(new GridBagLayout());
		MainPanel.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(),"<html><font color='white'>"+Translations.getTranslation("Statistics")));
		GridBagConstraints constraints = new GridBagConstraints();
		MainPanel.setOpaque(true);
		MainPanel.setBackground(Color.GRAY);
		MainPanel.setForeground(Color.BLACK);
		
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(10, 10, 10, 10);
		
		//SHOW HISTORY
		showHistory.addActionListener(e -> {ShowHistory();});
		new TextToTranslationJButton(showHistory,"history");
		constraints.gridx = 0;
		constraints.gridy = 1;
		showHistory.setBackground(Color.WHITE);
		showHistory.setForeground(Color.BLACK);
		MainPanel.add(showHistory,constraints);
		showHistory.setSelected(true); //CONTROLLARE PERCHE' IL BOTTONE RIMANE BLU 
		
		//CLOSE BUTTON
		close.addActionListener(e->{closeStatistics();});
		new TextToTranslationJButton(close, "close");
		constraints.gridx = 0;
		constraints.gridy = 2;
		close.setBackground(Color.WHITE);
		close.setForeground(Color.BLACK);
		MainPanel.add(close,constraints);
		
		//FRAME OPTIONS
        this.setResizable(true);
        add(MainPanel);
        //setSize(800, 600);
        pack();
        setVisible(true);
	}
	
	/**
	 * method to open a new JPanel with the history of the matches
	 */
	private void ShowHistory() {
		try {
			this.remove(MainPanel);
			
			//MAIN PANEL SETUP
			MainPanel = new JPanel(new GridBagLayout());
			MainPanel.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(),"<html><font color='white'>"+Translations.getTranslation("Statistics")));
			GridBagConstraints constraints = new GridBagConstraints();
			MainPanel.setOpaque(true);
			MainPanel.setBackground(Color.GRAY);
			MainPanel.setForeground(Color.BLACK);
			
			constraints.anchor = GridBagConstraints.CENTER;
			constraints.insets = new Insets(10, 10, 10, 10);
			
			int height=0;
			
			HistoryImpl hist =HistoryImpl.deserialize();	
			
			for (height=0; height<hist.matches.size(); height++){

				//HISTORY ELEMENT
				constraints.gridx = 0;
				constraints.gridy = height;
				JLabel history_element= new JLabel();
				history_element.setText("Winner: "+hist.matches.get(height).getWinner()+" with " + hist.matches.get(height).getWinnerScore() + " points!"
						+ "The opponent was " + hist.matches.get(height).getLoser() + " he made " + hist.matches.get(height).getLoserScore() + " points."
						+ "Difficulty: " + hist.matches.get(height).getDiff()
						+ "\n\tDate of the game: " + hist.matches.get(height).getDate());
				MainPanel.add(history_element,constraints);
				title.setForeground(Color.white);
			}
			
			if (height>0){
				//CLEAR BUTTON
				clear.addActionListener(e->{try {
					hist.deleteHistory();
				} catch (ClassNotFoundException | IOException e1) {
					e1.printStackTrace();
				}ShowHistory();});
				new TextToTranslationJButton(clear, "clear");
				constraints.gridx = 0;
				constraints.gridy = height;
				clear.setBackground(Color.WHITE);
				clear.setForeground(Color.BLACK);
				MainPanel.add(clear,constraints);
				height++;
			}
			else{
				new TextToTranslationJLabel(title, "nohistory");
				constraints.gridx = 0;
				constraints.gridy = height;
				title.setBackground(Color.WHITE);
				title.setForeground(Color.BLACK);
				MainPanel.add(title,constraints);
				height ++;
			}

			//CLOSE BUTTON
			close.addActionListener(e->{closeStatistics();});
			new TextToTranslationJButton(close, "close");
			constraints.gridx = 0;
			constraints.gridy = height;
			close.setBackground(Color.WHITE);
			close.setForeground(Color.BLACK);
			MainPanel.add(close,constraints);
			
			//FRAME OPTIONS
	        add(MainPanel);
	        pack();
	        
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * method to close the panel of the statistics
	 */
	private void closeStatistics() {
		setVisible(false);
	}
}
