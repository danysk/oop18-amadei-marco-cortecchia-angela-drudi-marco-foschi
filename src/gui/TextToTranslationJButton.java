package gui;
/**
 * 
 * @author Marco Amadei
 * 
 * Classe per mantenere aggiornata una traduzione in caso di cambio di lingua (per i JButton)
 *
 */
import javax.swing.JButton;


public class TextToTranslationJButton extends TextToTranslation {
	private JButton button;

	public TextToTranslationJButton(JButton button, String translation_id){
		this.button=button;
		setTranslation(translation_id);
	}

	@Override 
	public void updateTranslation() {
		button.setText(getTranslation());
	}

}
