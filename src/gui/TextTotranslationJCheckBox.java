package gui;
/**
 * 
 * @author Marco Amadei
 * 
 * Classe per mantenere aggiornata una traduzione in caso di cambio di lingua (per i JCheckBox)
 *
 */
import javax.swing.JCheckBox;

public class TextTotranslationJCheckBox extends TextToTranslation {

	private JCheckBox checkbox;

	public TextTotranslationJCheckBox(JCheckBox checkbox, String translation_id)
	{
		this.checkbox=checkbox;
		setTranslation(translation_id);
	}

	@Override
	public void updateTranslation() 
	{
		checkbox.setText(getTranslation()); 
	}
}
