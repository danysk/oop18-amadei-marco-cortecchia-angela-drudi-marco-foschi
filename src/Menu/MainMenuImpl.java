package Menu;

import gui.MenuGUI;

/**
 * 
 * @author Angela Cortecchia
 *
 */
public class MainMenuImpl implements MainMenu {

	public boolean game;
	
	/**
	 * @param newgame
	 * @return boolean
	 */
	public boolean newGame(final boolean newgame) {
		return this.game = newgame;
	}

	public static void main(String[] args) {
		
		new MenuGUI();
	}
}
