package ai;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import javax.swing.JTextArea;

/**
 * HOW TO:
 * Just instantiate an object
 * and call the single public method
 */
public class AIThinkingFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel mainPanel = new JPanel(new ColumnLayout());
	//private JTextArea testo = new JTextArea("AI Thinking");
	private JLabel gifComponent = null;
	
	private enum GIF {
		BATMAN("http://giphygifs.s3.amazonaws.com/media/a5viI92PAF89q/200w_d.gif"),
		GORILLA("http://giphygifs.s3.amazonaws.com/media/xUPGcmF2iGsTGEFVL2/200w_d.gif"),
		LEONI("http://giphygifs.s3.amazonaws.com/media/DfSXiR60W9MVq/200w_d.gif"),
		COMPUTER("http://giphygifs.s3.amazonaws.com/media/WxJLwDBAXDsW1fqZ3v/200w_d.gif");
		
		private final String link;
		GIF(String link){ this.link = link; }
				
		public static GIF getRandomGif() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }

	}
	
	public AIThinkingFrame() {	
        //mainPanel.add(testo);
                
		this.add(mainPanel);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(250, 230);
	}
	

	public Thread showDelayAndDestroyARandomGifThread(int delay) throws InterruptedException, MalformedURLException {
		return new Thread() {
			public void run() {
				try{
					loadRandomGif();
					mainPanel.add(gifComponent);
					showDelayDestroy(delay);
					mainPanel.remove(gifComponent);
				}catch(Exception e) {}
			}
		};

	}
	public void showDelayAndDestroyARandomGif(int delay) throws InterruptedException, MalformedURLException {
		loadRandomGif();
		mainPanel.add(gifComponent);
		showDelayDestroy(delay);
		mainPanel.remove(gifComponent);
	}
	

	// METODI PRIVATI
	private void loadRandomGif() throws MalformedURLException {
		loadGif(GIF.getRandomGif());
	}
	
	private void showDelayDestroy(int delay) throws InterruptedException {
		this.showGif();
		TimeUnit.SECONDS.sleep(delay);
		this.destroyGif();
	}
	
	private void loadGif(GIF gif) throws MalformedURLException {
		URL url = new URL(gif.link);
		Icon icon = new ImageIcon(url);
        gifComponent = new JLabel(icon);
	}
	
	private void showGif() {
		setVisible(true);
	}
	
	private void destroyGif() {
		this.dispose();
	}
}
