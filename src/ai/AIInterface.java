package ai;

import java.net.MalformedURLException;
import ai.enums.Option;
import ai.enums.Query;
import ai.enums.QueryType;
import ai.enums.Variant;
import ai.exceptions.StockfishInitException;
import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;
import settings.MatchSettings.Difficulty;


public class AIInterface {

	
	/**
	 * 
	 * @param board on which to calculate the move
	 * @param color of the player that has to be moved
	 * @param difficulty that the ai has to use
	 * @param whiteCanShortCastle - true if the white can still short castle
	 * @param whiteCanLongCastle - true if the white can still long castle
	 * @param blackCanShortCastle - true if the black can still short castle
	 * @param blackCanLongCastle - true if the black can still long castle
	 * @param behindCaptureEnPassant - the BoardPosition behind a pawn that has just done its double step, null else
	 * @param numberOfMoves - counting from 0 incremented every time a player moves
	 * @param showThinkingFrame - true to show a thinking gif frame
	 * @return a couple of BoardPosition(s) from/to
	 * @throws StockfishInitException
	 */
	public Move calculateMove(
			GenericBoard board, 
			PlayerColor color, 
			Difficulty difficulty, 
			boolean whiteCanShortCastle,
			boolean whiteCanLongCastle,
			boolean blackCanShortCastle,
			boolean blackCanLongCastle,
			BoardPosition behindCaptureEnPassant,
			int numberOfMoves,
			boolean showThinkingFrame) throws StockfishInitException, MalformedURLException, InterruptedException{
		
		if (showThinkingFrame) openThinkingGIF();
				
		String boardToString = board.stringifyStateForAI1v1(
				color,
				whiteCanShortCastle, whiteCanLongCastle, 
				blackCanShortCastle, blackCanLongCastle,
				behindCaptureEnPassant,numberOfMoves);
		
		int aIskill=3;
		switch (difficulty) {
			case EASY: 
				aIskill=4; break;
			case HARD:
				aIskill=18; break;
			case NORMAL:
				aIskill=10; break;
		}
		
		StockfishClient client = new StockfishClient.Builder()
		        .setPath(getClass().getResource("/ai/engine/").getFile())
		        .setOption(Option.Skill_Level, aIskill) // Stockfish skill level 0-20
		        .setVariant(Variant.DEFAULT)
		        .build();
		Query query = new Query.Builder(QueryType.Best_Move)
		        .setFen(boardToString).setDepth(6)
		        .build();
		
		return StockfishClient.convertStringToMove(client.thinkOfAMove(query));
	}
	
	/**
	 * Open a thinking gif GUI
	 */
	public void openThinkingGIF()
	{
		AIThinkingFrame aitf = new AIThinkingFrame();
		aitf.setVisible(true);
		try {
			aitf.showDelayAndDestroyARandomGifThread(3).start();
		} catch (MalformedURLException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
