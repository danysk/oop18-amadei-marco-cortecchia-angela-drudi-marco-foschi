/* Copyright 2018 David Cai Wang
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ai;


import ai.enums.Option;
import ai.enums.Query;
import ai.enums.Variant;
import ai.exceptions.StockfishEngineException;
import ai.exceptions.StockfishInitException;
import ai.stockfish.Stockfish;
import board.BoardPosition;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * this class is part of <a href="https://github.com/Niflheim/Stockfish-Java"> a GitHub project </a>
 * it has just been adapted to work better in our software
 * 
 * i just added the possibility to run the "decision" in a sync way
 * because for our purpose the async was not necessary
 */
public class StockfishClient {
    private ExecutorService executor, callback;
    private Queue<Stockfish> engines;
    
    private Stockfish monoEngine;
    

    public StockfishClient(String path, int instances, Variant variant, Set<Option> options) throws StockfishInitException {
        executor = Executors.newFixedThreadPool(instances);
        callback = Executors.newSingleThreadExecutor();
        engines = new ArrayBlockingQueue<Stockfish>(instances);
        
        monoEngine = new Stockfish(path, variant, options.toArray(new Option[options.size()]));

        for (int i = 0; i < instances; i++)
            engines.add(new Stockfish(path, variant, options.toArray(new Option[options.size()])));
    }
    
    
    /**
     * WARNING! DO NOT USE THIS UNLESS YOU ARE GOD!
     */
    public void submit(Query query) {
        submit(query, null);
    }
    
    /**
     * WARNING! DO NOT USE THIS UNLESS YOU ARE GOD!
     */
    public void submit(Query query, Consumer<String> result) {
    	executor.submit(() -> {
            Stockfish engine = engines.remove();
            String output;

            switch (query.getType()) {
                case Best_Move: output = engine.getBestMove(query); break;
                case Make_Move: output = engine.makeMove(query); break;
                case Legal_Moves: output = engine.getLegalMoves(query); break;
                case Checkers: output = engine.getCheckers(query); break;
                default: output = null; break;
            }
            
            callback.submit(() -> result.accept(output));
            engines.add(engine);
        });
    }
    
    public String thinkOfAMove(Query query) {    	
    	switch (query.getType()) {
	        case Best_Move: return monoEngine.getBestMove(query); 
	        case Make_Move: return monoEngine.makeMove(query); 
	        case Legal_Moves: return monoEngine.getLegalMoves(query); 
	        case Checkers: return monoEngine.getCheckers(query);  
	        default: return null;
    	}
    }

 
    
    public static BoardPosition convertStringToOriginPosition(String rawMove) throws StockfishEngineException {
    	String rawFrom = rawMove.substring(0, 2);	
    	BoardPosition from = new BoardPosition(
    			rawFrom.substring(0,1), Integer.valueOf( rawFrom.substring(1,2) ) );
    	    	
    	return from;
    }
    
    public static BoardPosition convertStringToDestinationPosition(String rawMove) throws StockfishEngineException {
    	String rawTo = rawMove.substring(2,4);
    	BoardPosition to = new BoardPosition(
    			rawTo.substring(0,1), Integer.valueOf( rawTo.substring(1,2)   ) );
    	    	
    	return to;
    }
    
    public static Move convertStringToMove(String rawMove) throws StockfishEngineException {    	    	
    	BoardPosition from = convertStringToOriginPosition(rawMove);
    	BoardPosition to   = convertStringToDestinationPosition(rawMove);
    	    	
    	return new Move(from,to);
    }
    

	public static class Builder {
        private Set<Option> options = new HashSet<>();
        private Variant variant = Variant.DEFAULT;
        private String path = null;
        private int instances = 1;

        public final Builder setInstances(int num) {
            instances = num;
            return this;
        }

        public final Builder setVariant(Variant v) {
            variant = v;
            return this;
        }

        public final Builder setOption(Option o, long value) {
            options.add(o.setValue(value));
            return this;
        }

        public final Builder setPath(String path) {
            this.path = path;
            return this;
        }

        public final StockfishClient build() throws StockfishInitException {
            return new StockfishClient(path, instances, variant, options);
        }
    }
}
